#!/usr/bin/env python3 

from flask import Flask, render_template, send_from_directory, request, url_for
from flask_socketio import SocketIO, emit, join_room

from apscheduler.schedulers.background import BackgroundScheduler as BackgroundScheduler

from Timer import Timer

app = Flask('countdown', template_folder='web', static_url_path='')
app.config['SECRET_KEY'] = 'secret!_mnn62zds7if9rkye3kvc73lkefi3foz8qe59'
socketio = SocketIO(app)

scheduler = BackgroundScheduler()

data = Timer()


@app.after_request
def add_header(r):
    """
    Add response headers to disable client-side caching to every response.
    :param r: response to add headers.
    :return: response with added headers.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.route('/js/<path:path>')
def send_js(path):
    """
    This function serves JS files from /web/js/ folder.
    :param path: Path to file extracted from URL.
    :return: File retrieved from folder.
    """
    return send_from_directory('web/js', path)


@app.route('/')
def user_index():
    """
    Returns user countdown window.
    :return: countdown web page.
    """
    return render_template('index.html')


@app.route('/admin')
def admin_index():
    """
    Returns admin control panel.
    :return: Admin control web page.
    """
    return render_template('admin.html', formats=data.available_formats)


@socketio.on('connection')
def handle_connection(json):
    print('received connection json: ' + str(json))

    # Subscribe all clients to countdown time updates.
    join_room('countdown')


@socketio.on('adminAction')
def handle_adminAction(json):
    print('received action: ' + str(json))
    if json["action"] == "start":
        data.start()
    elif json["action"] == "stop":
        data.stop()
    elif json["action"] == "set":
        data.set_time(int(json["dd"]) * 86400 +
                      int(json["hh"]) * 3600 +
                      int(json["mm"]) * 60 +
                      int(json["ss"]))
    elif json["action"] == "add":
        data.add_time(int(json["dd"]) * 86400 +
                      int(json["hh"]) * 3600 +
                      int(json["mm"]) * 60 +
                      int(json["ss"]))
    elif json["action"] == "subtract":
        data.add_time(int(json["dd"]) * 86400 +
                      int(json["hh"]) * 3600 +
                      int(json["mm"]) * 60 +
                      int(json["ss"]) * -1)
    elif json["action"] == "set_format":
        data.set_format(json["format"])
    elif json["action"] == "set_text":
        data.set_texts(json["top_text"], json["bottom_text"])
    elif json["action"] == "hide":
        data.set_hidden(True)
    elif json["action"] == "show":
        data.set_hidden(False)


@scheduler.scheduled_job('interval', seconds=1)
def broadcast_time():
    data.tick()
    json = data.get_json()
    socketio.emit('gameData', json, room='countdown')


if __name__ == '__main__':
    scheduler.start()
    # scheduler.add_job(broadcastPing, 'interval', seconds=3)
    try:
        # socketio.run(app, debug=True, use_reloader=False)
        socketio.run(app, debug=True, use_reloader=False, host='0.0.0.0')
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()
