var time_format = null;

/**
 * Depending on received status of countdown, updates "start" and "stop" buttons.
 * @param status {boolean}: whether the countdown is running or not.
 */
function update_start_stop_buttons(status) {
    // TODO think about hiding the elements altogether (instead of just disabling it).
    if(status == false) {
        document.getElementById("start").disabled = false;
        document.getElementById("stop").disabled = true;
    }
    else {
        document.getElementById("start").disabled = true;
        document.getElementById("stop").disabled = false;
    }
}

/**
 * Depending on received status of countdown, updates "hide" and "show" buttons.
 * @param status {boolean}: whether the countdown is hidden or not.
 */
function update_hide_show_buttons(status) {
    // TODO think about hiding the elements altogether (instead of just disabling it).
    if(status == false) {
        document.getElementById("hide").disabled = false;
        document.getElementById("show").disabled = true;
    }
    else {
        document.getElementById("hide").disabled = true;
        document.getElementById("show").disabled = false;
    }
}

/**
 * Updates time_format selector to currently selected time_format.
 * @param received_format {string}: Currently received time format.
 */
function update_format(received_format) {
    if(time_format != received_format) {
        var select = document.getElementById("select_format");
        var option_id = "format_"+received_format;

        select.options[select.selectedIndex].selected = false;
        document.getElementById(option_id).selected = true;
        time_format = received_format;

        update_time_inputs(received_format)
    }
}


/**
 * Enable (and show) time input field.
 * @param current {HTMLInputElement}: Field to enable
 * @param previous {HTMLInputElement}: Next field (smaller unit) than current field.
 * @param max_value {int}: Number of previous units in this element's unit.
 * @param summary_seconds {int}: time in seconds, that should be displayed in this and smaller fields.
 * @param seconds_in_interval {int}: how many seconds is 1 unit of current field
 * @return {int}: summary_seconds for the rest of fields (without this one).
 */
function enable_field(current, previous, max_value, summary_seconds, seconds_in_interval) {
    current.disabled=false;
    current.classList.remove("hidden");

    previous.classList.add("time_input");
    previous.classList.remove("unlimited_time_input");

    var current_value = Math.floor(summary_seconds/seconds_in_interval);
    current.value = current_value;

    previous.max=max_value-1;

    return summary_seconds-current_value*seconds_in_interval;
}

/**
 * Disables (and hides) time input field. Also sets value of this field to 0.
 * @param current {HTMLInputElement}: Field to enable
 * @param previous {HTMLInputElement}: Next field (smaller unit) than current field.
 * @param max_value {int}: Number of previous units in this element's unit.
 */
function disable_field(current, previous, max_value) {
    previous.max=null;

    previous.classList.add("unlimited_time_input");
    previous.classList.remove("time_input");

    current.disabled=true;
    current.classList.add("hidden");

    var new_value = parseInt(previous.value) + parseInt(current.value) * max_value;

    previous.value = new_value.toString()
    current.value="0";
}

/**
 * Updates time input fields in case of time format change.
 * When invoked, hides/shows fields and changes format of already filled values to match new time format.
 */
function update_time_inputs() {
    var days = document.getElementById("days");
    var hrs = document.getElementById("hrs");
    var mins = document.getElementById("mins");
    var secs = document.getElementById("secs");

    var days_colon = document.getElementById("days_colon");
    var hrs_colon = document.getElementById("hrs_colon");
    var mins_colon = document.getElementById("mins_colon");

    var summary_seconds = parseInt(days.value)*86400
                          + parseInt(hrs.value)*3600
                          + parseInt(mins.value)*60
                          + parseInt(secs.value);

    if(time_format == "dd:hh:mm:ss") {
        summary_seconds = enable_field(days, hrs, 24, summary_seconds, 86400);
        days_colon.classList.remove("hidden");
    } else {
        disable_field(days, hrs, 24);
        days_colon.classList.add("hidden");
    }

    if(time_format == "dd:hh:mm:ss" || time_format == "hh:mm:ss") {
        summary_seconds = enable_field(hrs, mins, 60, summary_seconds, 3600);
        hrs_colon.classList.remove("hidden");
    } else {
        disable_field(hrs, mins, 60);
        hrs_colon.classList.add("hidden");
    }

    if(time_format == "dd:hh:mm:ss" || time_format == "hh:mm:ss" || time_format == "mm:ss") {
        summary_seconds = enable_field(mins, secs, 60, summary_seconds, 60);
        mins_colon.classList.remove("hidden");
    } else {
        disable_field(mins, secs, 60);
        mins_colon.classList.add("hidden");
    }

    secs.value = summary_seconds;
}

/**
 * Sends SocketIO "start" action.
 */
function start() {
    var payload = {action: 'start'};
    sendMessageOnce('adminAction', payload);
}

/**
 * Sends SocketIO "stop" action.
 */
function stop() {
    var payload = {action: 'stop'};
    sendMessageOnce('adminAction', payload);
}
/**
 * Sends SocketIO "hide" action.
 */
function hide() {
    var payload = {action: 'hide'};
    sendMessageOnce('adminAction', payload);
}

/**
 * Sends SocketIO "show" action.
 */
function show() {
    var payload = {action: 'show'};
    sendMessageOnce('adminAction', payload);
}

/**
 * Sends SocketIO  action - retrieves time from admin form and sends it with specified admin action
 * @param action: Admin action string identifier to send.
 */
function send_time_payload(action) {
    var payload = {action: action};

    payload["dd"] = document.getElementById("days").value;
    payload["hh"] = document.getElementById("hrs").value;
    payload["mm"] = document.getElementById("mins").value;
    payload["ss"] = document.getElementById("secs").value;

    sendMessageOnce('adminAction', payload);
}

/**
 * Sends SocketIO "set_format" action - sets time format to be shown to users
 */
function set_format() {
    var payload = {action: 'set_format'};
    var select = document.getElementById("select_format");
    payload["format"] = select.options[select.selectedIndex].value;

    sendMessageOnce('adminAction', payload);
}

/**
 * Sends SocketIO "set_text" action - sets texts above and below time.
 */
function set_text() {
    var payload = {action: 'set_text'};
    payload["top_text"] = document.getElementById("top_text").value;
    payload["bottom_text"] = document.getElementById("bottom_text").value;

    sendMessageOnce('adminAction', payload);
}
