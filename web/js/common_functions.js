/**
 * Updates time received from server into element specified by ID.
 * @param json {json}: JSON object received from server (containing "time" and "format" elements)
 * @param element_id {string}: Element ID to which time should be put as innerHTML.
 */
function update_time(json, element_id) {
    document.getElementById(element_id).innerHTML = time_to_string(json["time"], json["format"]);
}

/**
 * Converts time (in seconds) to specified formatted string.
 * @param time {int}: Time (in seconds) that should be returned.
 * @param received_format {string}: Format in which time should be returned.
 * @return {string}: Time (in specified format).
 */
function time_to_string(time, received_format) {
    var seconds = time;
    var result_string = "";

    if(received_format == "dd:hh:mm:ss") {
        var delta = Math.floor(seconds / 86400);
        if(delta>0) {
            result_string += delta.toString()+":";
            seconds = seconds - delta*86400;
        }
    } // endif days

    if(received_format == "dd:hh:mm:ss" || received_format == "hh:mm:ss") {
        var delta = Math.floor(seconds / 3600);

        result_string += delta.toString().padStart(2, '0')+":";
        seconds = seconds - delta*3600;
    } // endif hours

    if(received_format == "dd:hh:mm:ss" || received_format == "hh:mm:ss"
        || received_format == "mm:ss") {
        var delta = Math.floor(seconds / 60);

        result_string += delta.toString().padStart(2, '0')+":";
        seconds = seconds - delta*60;
    } // endif minutes

    // seconds
    result_string += seconds.toString().padStart(2, '0');

    return result_string;
}