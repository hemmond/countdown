class Timer:
    """
    This class is handling all time-related things.

    This class does not have any notion of real time, so it relies on every "tick" being triggered from outside.
    """

    def __init__(self):
        """
        Creates instance of Timer.
        """
        self.running = False
        self.time = 90 * 60  # default time (upon start) in seconds
        self.available_formats = ["ss", "mm:ss", "hh:mm:ss", "dd:hh:mm:ss"]
        self.format = self.available_formats[1]

        self.top_text = ""
        self.bottom_text = ""
        self.hidden = False

    def start(self):
        """
        Starts countdown timer.
        """
        self.running = True

    def stop(self):
        """
        Stops countdown timer.
        """
        self.running = False

    def tick(self):
        """
        Decreases countdown by one second, if countdown is running.
        """
        if self.running is True and self.time > 0:
            self.time -= 1

    def set_time(self, time):
        """
        Replaces currently used time with specified time.
        :param time: New time (in seconds) the countdown should show.
        """
        self.time = time

    def add_time(self, time):
        """
        Adds time to current time.
        :param time: time (in seconds) to add to self.time; for subtracting, let time be negative number
        """
        self.time += time

    def set_format(self, time_format):
        """
        Change time format in which the countdown should be shown if specified format is known.
        :param time_format: String specifying time format.
        """
        if str(time_format) in self.available_formats:
            self.format = str(time_format)

    def set_texts(self, top_text, bottom_text):
        """
        Changes texts shown above and below countdown.
        :param top_text: Text shown above the countdown.
        :param bottom_text: Text shown below the countdown.
        """
        self.top_text = str(top_text)
        self.bottom_text = str(bottom_text)

    def set_hidden(self, hidden: bool):
        """
        Shows/hides countdown and texts
        :param hidden: Boolean whether the countdown and texts should be hidden (true) or shown (false)
        """
        self.hidden = bool(hidden)

    def get_json(self):
        """
        Retrieve all necessary countdown data in the JSON format for frontend website.
        :return: dictionary of elements (to be converted to JSON).
        """
        payload = {"running": self.running,
                   "format": self.format,
                   "time": self.time,
                   "top_text": self.top_text,
                   "bottom_text": self.bottom_text,
                   "hidden": self.hidden}
        return payload
